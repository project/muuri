<?php

namespace Drupal\muuri_test\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class TestController.
 */
class TestController extends ControllerBase {

  /**
   * Test Muuri.
   *
   * @return array
   *   Markup.
   */
  public function testMuuri() {

    $markup = '<div class="grid">
    
      <div class="item">
        <div class="item-content">
          <!-- Safe zone, enter your custom markup -->
          This can be anything.
          <!-- Safe zone ends -->
        </div>
      </div>
    
      <div class="item">
        <div class="item-content">
          <!-- Safe zone, enter your custom markup -->
          <div class="my-custom-content">
            Yippee!
          </div>
          <!-- Safe zone ends -->
        </div>
      </div>
    
    </div>';

    return [
      '#markup' => $markup,
      '#attached' => [
        'library' => [
          'muuri/muuri',
        ],
      ],
    ];
  }

}
