<?php

namespace Drupal\muuri;

/**
 * Class MuuriService.
 *
 * @package Drupal\muuri
 */
class MuuriService {

  /**
   * Get Muuri options.
   *
   * @return array
   *   Array of options.
   */
  public function getOptions() {
    return [];
  }

  /**
   * Initialize Muuri.
   *
   * @param array $form
   *   Muuri render array.
   */
  public function init(array &$form) {
    $form['#attached']['library'][] = 'muuri/muuri';
    $form['#attached']['drupalSettings']['muuri']['elements'] = [];
  }

}
