<?php

namespace Drupal\muuri\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Style plugin to render each item into owl carousel.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "views_muuri",
 *   title = @Translation("Muuri"),
 *   help = @Translation("Displays rows as a Muuri layout."),
 *   theme = "views_muuri",
 *   display_types = {"normal"}
 * )
 */
class MuuriViews extends StylePluginBase {

  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // Get Muuri grid options.
    $settings = _muuri_default_grid_options();
    foreach ($settings as $key => $value) {
      $options[$key] = ['default' => $value];
    }
    return $options;
  }

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $this->messenger()
      ->addMessage($this->t('For more information about Muuri grid options and their accepted values visit <a href=":page" target="_blank"> Muuri docs page</a>.', [':page' => 'https://github.com/haltu/muuri#grid-options']));
    // ShowDuration Option.
    $form['showDuration'] = [
      '#type'          => 'number',
      '#title'         => t('Show duration'),
      '#min'           => 0,
      '#max'           => 2000,
      '#step'          => 100,
      '#default_value' => $this->options['showDuration'],
      '#description'   => t("Set the show duration grid option default value is 300."),
    ];
    // HideDuration option.
    $form['hideDuration'] = [
      '#type'          => 'number',
      '#title'         => t('Hide duration'),
      '#min'           => 0,
      '#max'           => 2000,
      '#step'          => 100,
      '#default_value' => $this->options['hideDuration'],
      '#description'   => t("Set the hide duration grid option default value is 300."),
    ];
    // LayoutOnResize Option.
    $form['layoutOnResize'] = [
      '#type'          => 'select',
      '#title'         => t('layout On Resize'),
      '#options'       => [
        'false' => $this->t('False'),
        0       => $this->t('True'),
        100     => 100,
        200     => 200,
        300     => 300,
      ],
      '#default_value' => $this->options['layoutOnResize'],
      '#description'   => t("Set the layoutOnResize grid option default value is 100."),
    ];
    // LayoutOnInit option.
    $form['layoutOnInit'] = [
      '#type'          => 'select',
      '#title'         => t('Layout on init'),
      '#options'       => [
        'false' => $this->t('False'),
        'true'  => $this->t('True'),
      ],
      '#default_value' => $this->options['layoutOnInit'],
      '#description'   => t("Set the layoutOnInit grid option default value is 100."),
    ];
    // LayoutDuration Option.
    $form['layoutDuration'] = [
      '#type'          => 'number',
      '#title'         => t('Layout Duration'),
      '#min'           => 0,
      '#max'           => 2000,
      '#step'          => 100,
      '#default_value' => $this->options['layoutDuration'],
      '#description'   => t("Set the layoutDuration grid option default value is 300."),
    ];
    // DragEnabled Option.
    $form['dragEnabled'] = [
      '#type'          => 'select',
      '#title'         => t('Enable Drag'),
      '#options'       => [
        'false' => $this->t('False'),
        'true'  => $this->t('True'),
      ],
      '#default_value' => $this->options['dragEnabled'],
      '#description'   => t("Set the dragEnabled grid option default value is false."),
    ];
    // DragContainer option.
    $form['dragContainer'] = [
      '#type'          => 'textfield',
      '#title'         => t('Drag Container'),
      '#default_value' => (string) $this->options['dragEnabled'],
      '#description'   => t("Set the dragContainer grid option default value is null, Accepted types: element (such as document.body), false=null."),
    ];
    // DragAxis Option.
    $form['dragAxis'] = [
      '#type'          => 'select',
      '#title'         => t('Drag Axis'),
      '#options'       => [
        'null' => $this->t('Null'),
        'x'    => $this->t('X'),
        'y'    => $this->t('Y'),
      ],
      '#default_value' => $this->options['dragAxis'],
      '#description'   => t("Set the dragEnabled grid option default value is false."),
    ];
    // DragSort Option.
    $form['dragSort'] = [
      '#type'          => 'select',
      '#title'         => t('Drag Sort'),
      '#options'       => [
        'true'  => $this->t('True'),
        'false' => $this->t('False'),
      ],
      '#default_value' => $this->options['dragSort'],
      '#description'   => t("Set the dragEnabled grid option default value is true."),
    ];
    // DragReleaseDuration Option.
    $form['dragReleaseDuration'] = [
      '#type'          => 'number',
      '#title'         => t('Drag Release Duration'),
      '#min'           => 0,
      '#max'           => 2000,
      '#step'          => 100,
      '#default_value' => $this->options['dragReleaseDuration'],
      '#description'   => t("Set the dragReleaseDuration grid option default value is 300."),
    ];
    // DragReleaseEasing Option.
    $form['dragReleaseEasing'] = [
      '#type'          => 'select',
      '#title'         => t('Drag Release Easing'),
      '#options'       => [
        'ease'        => $this->t('Ease'),
        'ease-out'    => $this->t('Ease-out'),
        'ease-in'     => $this->t('Ease-in'),
        'ease-in-out' => $this->t('Ease-in-out'),
      ],
      '#default_value' => $this->options['dragReleaseEasing'],
      '#description'   => t("Set the dragReleaseEasing grid option default value is ease."),
    ];
  }

}
