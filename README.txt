CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Creators
 * Maintainers

 INTRODUCTION
 ------------

Muuri module Provides integration with https://haltu.github.io/muuri .

 INSTALLATION
 ------------

  * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-8
    for further information.

 REQUIREMENTS
 ------------

  This module need no requirements.

 CONFIGURATION
 -------------

 CREATORS
 --------

 Creator:
 * Sang Lostrie (baikho) - https://www.drupal.org/u/baikho

  MAINTAINERS
  --------

 * Berramou - https://www.drupal.org/u/berramou
 * Sang Lostrie (baikho) - https://www.drupal.org/u/baikho
