/**
 * @file
 * Muuri behavior.
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.muuri = {
    attach: function attach(context, settings) {
      // Get Muuri grid options.
      var muuriGridOptions = drupalSettings.muuri.gridOptions;
      if (Muuri !== 'undefined') {
        new Muuri('.grid', muuriGridOptions);
      }
    },
    detach: function detach(context, settings, trigger) {
    }
  };

})(jQuery, Drupal, drupalSettings);
