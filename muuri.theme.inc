<?php

/**
 * @file
 * Preprocessors and helper functions to make theming easier.
 */

/**
 * Prepares variables for Views Masonry templates.
 *
 * Default template: views-muuri.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A View object.
 */
function template_preprocess_views_muuri(array &$variables) {
  // Get view handler.
  $handler = $variables['view']->style_plugin;
  // Get formatted grid option to send as drupal settings.
  $grid_options = _muuri_formatted_grid_options($handler->options);
  // Add Row / Grid class.
  $variables['attributes']['class'][] = 'grid';
  $variables['attributes']['class'][] = 'row';

  // Attach muuri library.
  $attached = [
    '#attached' => ['library' => ['muuri/muuri']],
  ];

  // Add Muuri grid options as settings to use in grid initialisation.
  $variables['#attached']['drupalSettings']['muuri']['gridOptions'] = $grid_options;

  // Render library.
  \Drupal::service('renderer')->render($attached);
  // Call the unformatted theme function to not duplicate the same code.
  template_preprocess_views_view_unformatted($variables);
}
